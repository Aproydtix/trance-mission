﻿using UnityEngine;
using System.Collections;

public class FractalBehavior : MonoBehaviour
{
    [Header("References")]
    public Material fractalMaterial = null;
    public RhythmEngine rhythmEngine = null;
    [Header("Zoom")]
    public float uniformZoom = 1f;
    public Vector2 zoomLimits;
    private float zoomSpeed = 1f;
    [Tooltip("Duration of each zoom period measured in seconds. ")]
    public float zoomAnimPeriod;
    private float zoomAnimTimer = 0f;
    public bool zoomAnimation = true;
    public bool zoomExponential = false;

    [Header("Pan")]
    [Range(0, 1)]
    public float panSensitivity = 1f;
    [Range(-1f, 1f)]
    public float panSliderX = 0f;
    [Range(-1f, 1f)]
    public float panSliderY = 0f;
    public Vector2 pan = Vector2.zero;

    [Header("Iterations")]
    public float iterationLerpFactor = 0.1f;
    public float iterationAnimDuration = 2f;
    private float iterationAnimTimer = 0f;
    private int iterationAnimDirection = 1;
    private float iterationNumber;
    public Vector2 iterationLimits = new Vector2(1, 100);
    public bool iterationAnimation = true;

	private void Start ()
	{
        if (fractalMaterial == null)
            fractalMaterial = GetComponent<Renderer>().material;
        if (rhythmEngine == null)
            rhythmEngine = GetComponent<RhythmEngine>();
	}

	private void Update ()
	{
        #region Controls
        // Sensitivity is dependent on the zoom level
        panSliderX *= (1f / uniformZoom) * panSensitivity;
        panSliderY *= (1f / uniformZoom) * panSensitivity;
        pan.x -= panSliderX;    // Reverse the direction
        pan.y += panSliderY;

        // Set pan and zoom
        fractalMaterial.SetVector("_Pan", new Vector4(pan.x, pan.y, 1, 1));
        if (uniformZoom != 0f)
            fractalMaterial.SetVector("_Zoom", new Vector4(1f / uniformZoom, 1f / uniformZoom, 1, 1));
        #endregion

        #region Iteration Animation
        if (iterationAnimation)
        {
            if (Input.GetButtonDown("Jump"))
            {
                // Trigger the animation timer to start animating
                iterationAnimTimer += Time.deltaTime * iterationAnimDirection * iterationLerpFactor;
            }

            // If animation timer is not at resting position...
            if (iterationAnimTimer > 0f && iterationAnimTimer < iterationAnimDuration)
            {
                iterationAnimTimer += Time.deltaTime * iterationAnimDirection;
                // Clamp beneath animation duration
                if (iterationAnimTimer >= iterationAnimDuration)
                {
                    iterationAnimTimer = iterationAnimDuration;
                    iterationAnimDirection = -iterationAnimDirection; // Switch direction
                }
                // Clamp above 0
                else if (iterationAnimTimer <= 0f)
                {
                    iterationAnimTimer = 0f;
                    iterationAnimDirection = -iterationAnimDirection; // Switch direction
                }

                // Set iterations
                float iterationNumber = iterationLimits.x + (iterationAnimTimer / iterationAnimDuration) * (iterationLimits.y - iterationLimits.x);
                fractalMaterial.SetFloat("_Iterations", iterationNumber);
            }
        }
        #endregion

        #region Zoom Animation
        if (zoomAnimTimer > 0f)
        {
            zoomAnimTimer += Time.deltaTime;
            if (zoomExponential)
                uniformZoom = zoomLimits.x + (zoomLimits.y - zoomLimits.x) * ((zoomAnimTimer * Mathf.Exp(zoomAnimTimer)) / zoomAnimPeriod);
            else
                uniformZoom = zoomLimits.x + (zoomLimits.y - zoomLimits.x) * (zoomAnimTimer / zoomAnimPeriod);

            if (zoomAnimTimer >= zoomAnimPeriod)
            {
                zoomAnimTimer = 0f;
                uniformZoom = zoomLimits.x;
            }
        }
        #endregion

        // Reset pan sliders to create a "spring" on the sliders. 
        // Don't hold the sliders for too long - they may behave unpredictably. 
        // Just quickly hold and release them several times. 
        panSliderX = panSliderY = 0f;
    }

    public void PlayZoom(Vector3 parameters)
    {
        zoomAnimTimer = Time.deltaTime;
        zoomLimits = new Vector2(parameters.x, parameters.y);
        uniformZoom = zoomLimits.x;
        zoomAnimPeriod = parameters.z;
    }

    public void PlayIterations(Vector3 parameters)
    {
        // Resetting values
        iterationNumber = parameters.z;
        iterationLimits.x = parameters.x;
        iterationLimits.y = parameters.y;

        iterationAnimTimer += Time.deltaTime * iterationAnimDirection * iterationLerpFactor;
    }
}