﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonScript : MonoBehaviour {

    public GameObject cursor;
    public bool draggable;
    public int functionID;
	public bool drag = false;
	public float clickTimer;
    public float clickDelay = 0.5f;
    public float clickOffset = 1.0f;
	
	private Ray ray;
	private RaycastHit hit;
    private Transform origin;

    public float grid = 0.5f;
    


    void Update()
    {
        Vector3 mousePos = cursor.GetComponent<CursorScript>().mousePos;

        float x = mousePos.x, y = mousePos.y;

        if (Input.GetMouseButtonDown(0))
        {
            ray = new Ray(mousePos, new Vector3(0, 0, 1));
            Physics.Raycast(ray, out hit);
            if (hit.collider != null)
                if (hit.collider.gameObject == gameObject)
                {
                    drag = true;
                    origin = transform;
                }
        }
        if (drag)
        {
            clickTimer += Time.deltaTime;
            if (draggable)
            {
                float reciprocalGrid = 1f / grid;

                x = Mathf.Round(mousePos.x * reciprocalGrid) / reciprocalGrid;
                y = Mathf.Round(mousePos.y * reciprocalGrid) / reciprocalGrid;


                if (Mathf.Abs((int)(mousePos.x - origin.position.x)) > clickOffset || Mathf.Abs((int)(mousePos.y - origin.position.y)) > clickOffset)
                    transform.position = new Vector3(x, y, transform.position.z);
            }
            if (Input.GetMouseButtonUp(0))
            {
                if (clickTimer < clickDelay && drag)
                {
                    if (functionID != -1) GetComponentInParent<MenuManager>().FunctionHandler(functionID, gameObject);
                }
                clickTimer = 0.0f;
                drag = false;
            }
        }
    }
}
