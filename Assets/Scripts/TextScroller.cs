﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextScroller : MonoBehaviour {
    public float scrollSpeed;
    public float offset;
    public Sprite[] titles;
    public GameObject BPM;

    private void Awake()
    {
        Cursor.visible = true;
    }

    void Update () {
        int index = BPM.GetComponent<BPMSelector>().BPMSelection;
        gameObject.GetComponentInChildren<UnityEngine.UI.Image>().sprite = titles[index];
        
        offset = (offset + scrollSpeed * Time.deltaTime * 60f) % 3000;
        transform.position = new Vector3(-offset*2 + 2*1000, transform.position.y, transform.position.z);
    }
}
