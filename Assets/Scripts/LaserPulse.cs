﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserPulse : MonoBehaviour {

    private ControllerScript controller;

    // Use this for initialization

    void Start ()
    {
        controller = GameObject.Find("Essentials").transform.GetChild(1).GetComponent<ControllerScript>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        float x = controller.x;
        transform.GetChild(0).transform.localScale = new Vector3(   
            1 + Mathf.Pow(x - 0.1f, 10)/8f,                                                    
            1 + Mathf.Pow(x - 0.1f, 10)/2f,                                                   
            1);
	}
}
