﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogoScript : MonoBehaviour {

    public float period;
    public float amplitude;

    void Update()
    {
        Vector3 offset = new Vector3(0, Mathf.Cos(Time.time * period) * amplitude, 0);
        gameObject.transform.position += offset;
    }
}
