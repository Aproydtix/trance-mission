﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour {

    public Camera camera;
    public GameObject controller;
    public GameObject background;
    public Sprite background2;
    public GameObject fractal;
    public float levelSize;
    public Vector2 target = Vector2.zero;

    // Use this for initialization
    void Start ()
    {/*
        //background changes
        if (controller.GetComponent<RhythmEngine>().BPM > 120)
        {
            background.GetComponent<SpriteRenderer>().sprite = background2;
        }
        */
    }
	
	// Update is called once per frame
	void Update ()
    {
        camera.orthographicSize = levelSize;
        if (!controller.GetComponent<ControllerScript>().gameComplete)
            background.transform.localScale = Vector3.one * (levelSize/12) * .56f * (1 + controller.GetComponent<ControllerScript>().x / 25f);
        fractal.transform.localScale = new Vector3(44 * (levelSize/12), 25 * (levelSize/12), 1);

        if (controller.GetComponent<ControllerScript>().dead && controller.GetComponent<ControllerScript>().arms.Count > 0)
            target = controller.GetComponent<ControllerScript>().arms[controller.GetComponent<ControllerScript>().arms.Count - 1].joint.transform.position;
        transform.position = Vector3.Lerp(transform.position, new Vector3(target.x, target.y, -10f), controller.GetComponent<RhythmEngine>().BPM/30F*Time.deltaTime);

    }

    void Screenshake()
    {

    }
}
