﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class GridScript : MonoBehaviour {

    ControllerScript controller;
    float l;

	// Use this for initialization
	void Awake ()
    {
        controller = GetComponent<ControllerScript>();
        l = controller.armLength;
	}

    // Update is called once per frame
    
    void Update ()
    {
        for (int i = 0; i < 5; i++)
        {
            Debug.DrawLine(new Vector2(-Camera.main.orthographicSize*(16f/9f), (-5/2+i) * l*2f), new Vector2(+Camera.main.orthographicSize*(16f/9f), (-5/2+i) * l*2f), Color.black);
        }

        for (int i = 0; i < 9; i++)
        {
           Debug.DrawLine(new Vector2((-9/2+i) * l*2f, -Camera.main.orthographicSize), new Vector2((-9/2+i) * l*2f, +Camera.main.orthographicSize), Color.black);
        }
    }
}
