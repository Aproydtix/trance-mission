﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecyclingScript : MonoBehaviour {

	void Start () {
        GetComponent<BoxCollider>().isTrigger = true;
        gameObject.AddComponent<Rigidbody>();
        GetComponent<Rigidbody>().useGravity = false;
	}
    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "GameIcon")
        {
            Cursor.visible = true;
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
        }
        Destroy(other.gameObject);
    }
}
