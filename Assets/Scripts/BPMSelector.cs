﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BPMSelector : MonoBehaviour {

    public int BPMSelection;
	// Use this for initialization
	void Awake () {
        DontDestroyOnLoad(transform.gameObject);
	}

    public int GetBPMSelection()
    { return BPMSelection; }

    public void SetBPMSelector(int selection)
    {
        BPMSelection = selection;
    }
    public void LoadSelectedLevel()
    {
        SceneManager.LoadScene("Level1");
    }
}
