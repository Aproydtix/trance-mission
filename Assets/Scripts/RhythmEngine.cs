﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class RhythmEngine : MonoBehaviour
{
    /*
        To do: 
        - make global GameObject to store musical progression data
    */

    [Header("Music Settings")]
    [SerializeField]
    public float BPM;
    [SerializeField]
    private float quarterNote;
    [SerializeField]
    [Tooltip("Use this to adjust startup gaps in music files. ")]
    private float offset;
    private float offsetAdditional;

    // Song Tracking
    public int songNumber;
    private float songPosition;
    private float songPositionDelta;
    private float lastHit;
    private float lastHitActually;
    private float nextBeatTime;
    private float nextBarTime;
    private float dspTimeSong;
    private int beatNumber = 0;
    private int barNumber = 0;
    private float lastBeat = 0f;
    public float armTimer = 0f;

    [Header("Difficulty Adjustment")]
    [SerializeField]
    [Tooltip("Accepted time margin. Increasing this value makes the game more forgiving. ")]
    private float timeMargin; 

    [Header("Fractal Settings")]
    [Tooltip("How many beats pass by before a fractal restarts animation? ")]
    public int beatsPerFractal = 4;
    public int fractalCounter = 0;
    public int fractalMode = 0;
    private int fractalDirection = 1;
    [SerializeField]
    private Vector2[] fractalZoomLimits;
    [SerializeField]
    private Vector2[] fractalIterationLimits;

    [Header("References")]
    [SerializeField]
    private AudioClip[] songs;
    [SerializeField]
    private float[] BPMList;
    [SerializeField]
    private AudioSource audioSource = null;
    [SerializeField]
    private GameObject[] musicalObjects;
    [SerializeField]
    private GameObject fractalObject = null;
    [SerializeField]
    private Text score;



    private void Awake ()
	{
        if (GameObject.FindGameObjectsWithTag("BPM").Length > 1)
            for (int i=0; i<GameObject.FindGameObjectsWithTag("BPM").Length-1; i++)
            Destroy(GameObject.FindGameObjectsWithTag("BPM")[i]);
        if (GameObject.Find("BPMSelector"))
            songNumber = GameObject.Find("BPMSelector").GetComponent<BPMSelector>().BPMSelection;

        // Setting up references
        if (audioSource == null)
            audioSource = GetComponent<AudioSource>();

        // Assigning and playing song based on songNumber
        audioSource.clip = songs[songNumber];
        audioSource.Play();

        // Measuring Unity-specific stuff. Don't remember what this is :P
        dspTimeSong = (float)AudioSettings.dspTime;

        // Initializing rhythmic values
        BPM = BPMList[songNumber];
        quarterNote = 60.0f / BPM;
        lastBeat = 0f;
	}

	private void Update ()
	{
        score.color = GetComponent<ControllerScript>().nyanColor;
        score.text = "Points: " + GetComponent<ControllerScript>().score
            + "\nCombo: " + GetComponent<ControllerScript>().combo
            + "\nBeats: " + GetComponent<ControllerScript>().beats
            + "\nTS: " + GetComponent<ControllerScript>().totalScore
            + "\nHiCom: " + GetComponent<ControllerScript>().highestCombo
            + "\nYeah: " + GetComponent<ControllerScript>().beatAccuracy[0]
            + "\nSexy: " + GetComponent<ControllerScript>().beatAccuracy[1]
            + "\nOBby: " + GetComponent<ControllerScript>().beatAccuracy[2];
        // Updating song position every frame
        songPosition = (float)(AudioSettings.dspTime - dspTimeSong) * audioSource.pitch - offset;
        armTimer = (songPosition - lastBeat) / quarterNote;

        // This runs automatically every beat
        if (songPosition > lastBeat + quarterNote)
        {
            // Updates beat progression
            lastBeat += quarterNote;

            // Sends signals to GameObjects in the array called musicalObjects, 
            // that may have a public void function called "public void React()". 
            foreach (GameObject obj in musicalObjects)
            {
                obj.SendMessage("React");
            }

            // Fractal handlig
            fractalCounter++;

            // Resets fractal
            if (fractalCounter >= beatsPerFractal)
            {
                // Checks if the fractal is supposed to run. 
                if (fractalObject != null && fractalCounter >= beatsPerFractal)
                {
                    // Must send parameters as one parameter. Using Vector3 for these three parameters. 
                    fractalObject.SendMessage("PlayZoom", new Vector3(
                        fractalZoomLimits[fractalMode].x,
                        fractalZoomLimits[fractalMode].y,
                        quarterNote * beatsPerFractal));

                    fractalObject.SendMessage("PlayIterations", new Vector3(
                        fractalIterationLimits[fractalMode].x,
                        fractalIterationLimits[fractalMode].y,
                        fractalIterationLimits[fractalMode].x));

                    fractalCounter = 0;
                }

                fractalCounter = 0;

                fractalMode += fractalDirection;
                if (fractalMode <= 0 || fractalMode >= fractalZoomLimits.Length)
                {
                    fractalDirection = -fractalDirection;
                    fractalMode += fractalDirection;    // Correct if out of array bounds
                }
            }
        }

        // Rhythm gameplay
        if (Input.GetButtonDown("Jump") || Input.GetKeyDown(KeyCode.Mouse0))
        {
            if (Mathf.Abs(songPosition - lastBeat)/quarterNote <= timeMargin
                || Mathf.Abs(songPosition - quarterNote - lastBeat)/quarterNote <= timeMargin)
            {
                GetComponent<ControllerScript>().accuracy = 1;
                if (Mathf.Abs(songPosition - lastBeat)/quarterNote <= timeMargin/2f
                || Mathf.Abs(songPosition - quarterNote - lastBeat)/quarterNote <= timeMargin/2f)
                    GetComponent<ControllerScript>().accuracy = 2;
                if (Mathf.Abs(songPosition - lastBeat)/quarterNote <= timeMargin/3f
                || Mathf.Abs(songPosition - quarterNote - lastBeat)/quarterNote <= timeMargin/3f)
                    GetComponent<ControllerScript>().accuracy = 3;
                // This block only runs if the player successfully hits a beat. 
                GetComponent<ControllerScript>().tap = true;
            }
            else
            {
                GetComponents<AudioSource>()[1].clip = GetComponent<ControllerScript>().sfx[0];
                GetComponents<AudioSource>()[1].Play();
            }
        }
	}
}