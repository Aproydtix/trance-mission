﻿using UnityEngine;
using System.Collections;

public class MusicalObject : MonoBehaviour
{
    private bool thing = true;

	private void Start ()
	{

	}

	private void Update ()
	{

	}

    public void React()
    {
        thing = !thing;
        if (thing)
            transform.localScale = new Vector3(2, 2, 2);
        else
            transform.localScale = new Vector3(1, 1, 1);
    }
}