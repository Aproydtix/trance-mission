﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public Sprite taskbar, menu, power, options, musicTitle, imagesTitle, backGround;
    public Sprite[] icons;
    public Sprite[] songTiles;
    public AudioClip[] clips;
    public Sprite[] imageTiles;
    public Sprite[] pictures;

    private GameObject bananaMenu, powerButton, settingsButton, myMusicWindow, myImageWindow;
    private GameObject[] songs;
    private GameObject[] images;

    public GameObject icon;
    public GameObject cursor;
    public AudioSource jukeBox;

	void Start ()
    {
        InstantiateSprite(taskbar, new Vector3(-256, -176, 21), new Vector3(1024, 32, 1), false, -1, new Vector3(0.5f, -0.5f, 0));
        InstantiateSprite(backGround, new Vector3(0, 8, 22), new Vector3(24, 24, 1), false, -1, new Vector3(0, 0, 0));

        int index = 0;
        bananaMenu = InstantiateSprite(menu, new Vector3(-256, -176, 20), new Vector3(32, 32, 1), false, index++, new Vector3(0.5f, -0.5f, 0));
        powerButton = InstantiateSprite(power, new Vector3(-256, -160, 20), new Vector3(32, 32, 1), false, index++, new Vector3(0.5f, 0.5f, 0));
        settingsButton = InstantiateSprite(options, new Vector3(-256, -144, 20), new Vector3(32, 32, 1), false, index++, new Vector3(0.5f, 0.5f, 0));
        BananaMenu();

        int offset = 160;
		foreach (Sprite sprite in icons)
        {
            GameObject newSprite = InstantiateSprite(sprite, new Vector3(-224, offset, 19), new Vector3(32, 32, 1), true, index++, Vector3.zero);
            if (newSprite.GetComponent<ButtonScript>().functionID == 3) newSprite.AddComponent<RecyclingScript>();
            if (newSprite.GetComponent<ButtonScript>().functionID == 6) newSprite.tag = "GameIcon";
            offset -= 64;
        }

        offset = 144;
        myMusicWindow = InstantiateSprite(musicTitle, new Vector3(-192, offset + 16, 15), new Vector3(32, 32, 1), true, index++, new Vector3(0.5f, 0, 0));
        myImageWindow = InstantiateSprite(imagesTitle, new Vector3(32, offset + 16, 15), new Vector3(32, 32, 1), true, index++, new Vector3(0.5f, 0, 0));
        int songIndex = 0, imageIndex = 0;
        songs = new GameObject[songTiles.Length];
        images = new GameObject[imageTiles.Length];
        foreach (Sprite sprite in songTiles)
        {
            GameObject song =InstantiateSprite(sprite, new Vector3(-192, offset, 15), new Vector3(32, 32, 1), false, index, new Vector3(0.5f, -0.5f, 0));
            song.transform.SetParent(myMusicWindow.transform);
                //  The following line is pure shenanigans on my side
            song.transform.localScale = new Vector3(song.transform.localScale.x, song.transform.localScale.y, songIndex + 0.1f);    
            songs[songIndex++] = song;
            offset -= 32;
        }
        index++;
        offset = 144;
        foreach (Sprite sprite in imageTiles)
        {
            GameObject image = InstantiateSprite(sprite, new Vector3(-192, offset, 15), new Vector3(32, 32, 1), false, index, new Vector3(0.5f, -0.5f, 0));
            image.transform.SetParent(myImageWindow.transform);
            images[imageIndex++] = image;
            offset -= 32;
        }

        MyMusic();
        MyImages();
    }
    GameObject InstantiateSprite(Sprite sprite, Vector3 pos, Vector3 scale, bool draggable, int functionID, Vector3 boxOffset)
    {
        GameObject newSprite = Instantiate(icon, pos, Quaternion.identity, this.transform);
        newSprite.transform.localScale = scale;
        newSprite.GetComponent<SpriteRenderer>().sprite = sprite;
        newSprite.GetComponent<ButtonScript>().cursor = cursor;
        newSprite.GetComponent<ButtonScript>().draggable = draggable;
        newSprite.GetComponent<ButtonScript>().functionID = functionID;
        newSprite.GetComponent<BoxCollider>().center = boxOffset;
        return newSprite;
    }

    public void FunctionHandler(int functionID, GameObject caller)
    {
        switch (functionID)
        {
            case -1:                                                break;
            case 0: BananaMenu();                                   break;
            case 1: PowerButton();                                  break;
            case 4: case 7: MyMusic();                              break;
            case 5: case 8: MyImages();                             break;
            case 6: StartGame();                                    break;
            case 9: PlayMusic((int)caller.transform.localScale.z);  break;
            case 10: break;

        }
    }

    void BananaMenu()
    {
        powerButton.GetComponent<SpriteRenderer>().enabled = !powerButton.GetComponent<SpriteRenderer>().enabled;
        powerButton.GetComponent<BoxCollider>().enabled = !powerButton.GetComponent<BoxCollider>().enabled;
        settingsButton.GetComponent<SpriteRenderer>().enabled = !settingsButton.GetComponent<SpriteRenderer>().enabled;
        settingsButton.GetComponent<BoxCollider>().enabled = !settingsButton.GetComponent<BoxCollider>().enabled;
    }

    void PowerButton()
    {
        Cursor.visible = true;
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
    void MyMusic()
    {
        myMusicWindow.GetComponent<SpriteRenderer>().enabled = !myMusicWindow.GetComponent<SpriteRenderer>().enabled;
        myMusicWindow.GetComponent<BoxCollider>().enabled = !myMusicWindow.GetComponent<BoxCollider>().enabled;
        foreach (GameObject tile in songs)
        {
            tile.GetComponent<SpriteRenderer>().enabled = !tile.GetComponent<SpriteRenderer>().enabled;
            tile.GetComponent<BoxCollider>().enabled = !tile.GetComponent<BoxCollider>().enabled;
        }
        jukeBox.Stop();
    }

    void MyImages()
    {
        myImageWindow.GetComponent<SpriteRenderer>().enabled = !myImageWindow.GetComponent<SpriteRenderer>().enabled;
        myImageWindow.GetComponent<BoxCollider>().enabled = !myImageWindow.GetComponent<BoxCollider>().enabled;
        foreach (GameObject tile in images)
        {
            tile.GetComponent<SpriteRenderer>().enabled = !tile.GetComponent<SpriteRenderer>().enabled;
            tile.GetComponent<BoxCollider>().enabled = !tile.GetComponent<BoxCollider>().enabled;
        }
    }

    void StartGame()
    {
        SceneManager.LoadScene("GameMenu");
    }
    void PlayMusic(int index)
    {
        jukeBox.clip = clips[index];
        jukeBox.Play();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            PowerButton();
    }
}   
