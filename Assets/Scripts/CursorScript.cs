﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorScript : MonoBehaviour {

    [HideInInspector]
    public Vector3 mousePos;

    // Use this for initialization
    void Start () {
        Cursor.visible = false;
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        mousePos = new Vector3((Input.mousePosition.x / Screen.width) * 576 - 288, (Input.mousePosition.y / Screen.height) * 384 - 192, 0.4f);
        
        transform.position = mousePos;
	}
}
