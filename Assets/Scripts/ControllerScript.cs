﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arm
{
    public float x = 0;
    public float y = 0;
    public float angle = 0;
    public GameObject joint;
    public GameObject arm;
    public int accuracy = 0;
}

public class TextScript : MonoBehaviour
{
    public Sprite text;
    public float duration;
    float currentDuration;
    public Vector3 position;

    void Start()
    {
        gameObject.AddComponent<SpriteRenderer>().sprite = text;
        transform.position = position;
        transform.localScale = Vector3.one * .75f;
        currentDuration = duration;
    }

    void Update()
    {
        if (currentDuration <= 0)
            Destroy(gameObject);
        currentDuration -= Time.deltaTime;
        GetComponent<SpriteRenderer>().color = new Color(1,1,1, currentDuration/duration);
        transform.position = new Vector3(position.x, position.y + (1 - (currentDuration/duration)), position.z);
    }
}

public class ExplosionScript : MonoBehaviour
{
    public Sprite[] explosions = new Sprite[15];
    public float duration;
    float currentDuration;
    public Vector3 position;

    void Start()
    {
        gameObject.AddComponent<SpriteRenderer>().sprite = explosions[explosions.Length-1];
        transform.position = position;
        transform.localScale = Vector3.one * .75f;
        currentDuration = duration;
    }

    void Update()
    {
        if (currentDuration <= 0)
            Destroy(gameObject);
        currentDuration -= Time.deltaTime;
        if (currentDuration > 0)
        gameObject.GetComponent<SpriteRenderer>().sprite = explosions[Mathf.FloorToInt((1-(currentDuration / duration)) * explosions.Length)];
        //GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, currentDuration / duration);
        transform.position = new Vector3(position.x, position.y + (1 - (currentDuration / duration)), position.z);
    }
}

public class ControllerScript : MonoBehaviour
{
    public int beats = 0;
    public int combo = 0;
    public int highestCombo = 0;
    public int[] beatAccuracy = new int[3];
    int rotations = 0;
    public int score = 0;
    public int totalScore = 0;
    public bool gameComplete = false;
    public int currentLevel = 0;
    GameObject levelObject;
    public GameObject[] levelPrefabs;
    public GameObject borders;
    public bool dead = false;
    public float bpm = 60;
    float turnRate = .2f;
    int turnDirection = -1;
    public int sideNumber = 6;  // Should be 4, 6, or 8
    public Vector2 startPos = Vector2.zero;
    public int startDirection = 0; // int from 0 to sideNumber-1;
    public Sprite[] joint;      
    public Sprite[] body;
    public Sprite head;
    GameObject headObject;
    public Sprite tail;
    int countDown = 8;
    public Sprite[] countDownNumbers;
    GameObject number;
    public Sprite[] accuracyText;

    public List<Arm> arms;
    public Arm currentArm;
    public float armLength = 2;

    public bool tap = false;
    public int accuracy = 0;
    public float x = 0;

    int currentPos = 0;
    int currentAngleGoal = 0;

    public Sprite background;

    public Color nyanColor;                                //NyanCat Colour
    Color[] nyanColors = new Color[6];                     //NyanCat Colour components
    bool glow = false;

    public AudioClip[] sfx;
    public AudioClip[] winSounds;
    int winStage = 0;
    GameObject reverb;
    public Sprite[] explosions;
    public Sprite pixel;
    GameObject filter;

    // Use this for initialization
    void Start ()
    {
        arms = new List<Arm>();
        NewArm();
        currentPos = startDirection; // Black Magic
        NewArm();

        bpm = GetComponent<RhythmEngine>().BPM;
        turnRate = bpm / 60f;

        arms[0].arm.transform.position = new Vector3(currentArm.x + Mathf.Cos(currentArm.angle) * armLength*1.8f, currentArm.y + Mathf.Sin(currentArm.angle) * armLength*1.8f, 1f);
        headObject = new GameObject();
        headObject.name = "Head";
        headObject.transform.position = new Vector3(currentArm.x + Mathf.Cos(currentArm.angle) * armLength * 2f, currentArm.y + Mathf.Sin(currentArm.angle) * armLength * 2f, -1f);
        headObject.transform.eulerAngles = currentArm.arm.transform.eulerAngles;
        headObject.AddComponent<SpriteRenderer>().sprite = head;

        nyanColors[0] = Color.red;                             //Red
        nyanColors[1] = (Color.red + Color.yellow) / 2f;       //Orange
        nyanColors[2] = Color.yellow;                          //Yellow
        nyanColors[3] = Color.green;                           //Green
        nyanColors[4] = Color.blue;                            //Blue
        nyanColors[5] = (Color.blue + Color.red) / 2f;         //Purple

        number = new GameObject();
        number.AddComponent<SpriteRenderer>();
        number.transform.position = new Vector3(0,0,-2);

        filter = new GameObject();
        filter.name = "Filter";
        filter.AddComponent<SpriteRenderer>().sprite = pixel;
        filter.transform.localScale = Vector3.one * Camera.main.orthographicSize * 500f;
        filter.transform.position = new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, -9f);

        LoadNextLevel();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.LoadLevel(0);

        if (Input.GetKeyDown(KeyCode.R))
            ResetGame();

        if (gameComplete)
        {
            GameWin();
            return;
        }

        if (Input.GetKeyDown(KeyCode.W))
            GameWin();

        x = GetComponent<RhythmEngine>().armTimer;
        if (x < 0)
            x = Mathf.Abs(x) % 1;
        if (x >= 1)
        {
            currentPos += turnDirection;
            currentPos = currentPos % sideNumber;
            currentAngleGoal = currentPos + turnDirection;
            x--;
            countDown--;

            if (dead)
            {
                Death();
                return;
            }

            if (countDown < 0)
            {
                beats++;
                rotations++;
                if (rotations >= sideNumber)
                {
                    combo = 0;
                    if (rotations >= sideNumber*3)
                    {
                        Death();
                    }
                }
            }
            else if (countDown < 5 && countDown >= 1)
            {
                GetComponents<AudioSource>()[1].clip = sfx[1];
                GetComponents<AudioSource>()[1].Play();
            }
        }

        if (dead)
            return;

        headObject.transform.localScale = Vector3.one * .4f * (1 + x / 25f);
        //Debug.Log(x);
        nyanColor = nyanColors[Mathf.FloorToInt(x*6)] * (1f - (x*6 - Mathf.FloorToInt(x*6))) + nyanColors[Mathf.FloorToInt(x*6 + 1) % 6] * (x*6 - Mathf.FloorToInt(x*6));
        if (arms.Count > 2)
            glow = true;
        for (int i = 1; i < arms.Count-1; i++)
            if (arms[i].accuracy != 3)
                glow = false;

        GameObject[] pickUps = GameObject.FindGameObjectsWithTag("PickUp");
        if (pickUps.Length > 0)
            for (int i = 0; i < pickUps.Length; i++)
            {
                pickUps[i].transform.localScale = Vector3.one * .4f * (1 + x / 10f);
                pickUps[i].GetComponent<SpriteRenderer>().color = new Color((nyanColor.r+4)/5, (nyanColor.g+4)/5, (nyanColor.b+4)/5, 1);
            }

        GameObject goal = GameObject.FindGameObjectWithTag("Goal");
        if (goal)
        {
            goal.transform.localScale = Vector3.one * .4f * (1 + x / 10f);
            goal.GetComponent<SpriteRenderer>().color = new Color((nyanColor.r + 4) / 5, (nyanColor.g + 4) / 5, (nyanColor.b + 4) / 5, 1);
        }

        headObject.GetComponent<SpriteRenderer>().color = nyanColor;
        number.GetComponent<SpriteRenderer>().color = nyanColor;
        for (int i = 0; i < arms.Count; i++)
        {
            arms[i].arm.transform.GetChild(0).GetComponent<SpriteRenderer>().color = new Color(nyanColor.r, nyanColor.g, nyanColor.b, ((glow) ? 1f : .5f));
        }
        currentArm.joint.GetComponent<SpriteRenderer>().color = nyanColor;

        for (int i = 0; i < arms.Count; i++)
        {
            arms[i].arm.transform.localScale = Vector3.one * .5f * (1 + x / 25f);
            arms[i].arm.transform.GetChild(0).localScale = new Vector3((1 + x / 10f), (1 + x / 2f), 1);
            arms[i].joint.transform.localScale = Vector3.one * .25f * (1 + x / 10f);
        }


        if (countDown > 0)
        {
            number.SetActive(true);
            tap = false;
            filter.transform.position = new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, -9f);
            filter.GetComponent<SpriteRenderer>().color = Color.Lerp(filter.GetComponent<SpriteRenderer>().color, Color.clear, 1f/2f*(bpm/60f)*Time.deltaTime);
            if (countDown < 5)
            {
                number.GetComponent<SpriteRenderer>().sprite = countDownNumbers[countDown - 1];
                number.transform.position = new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, -2);
                filter.GetComponent<SpriteRenderer>().color = Color.clear;
            }
            return;
        }
        else
        {
            number.SetActive(false);

            if (currentArm.joint.transform.position.x > Camera.main.transform.position.x + Camera.main.orthographicSize * 16f / 9f
         || currentArm.joint.transform.position.x < Camera.main.transform.position.x - Camera.main.orthographicSize * 16f / 9f
         || currentArm.joint.transform.position.y > Camera.main.transform.position.y + Camera.main.orthographicSize
         || currentArm.joint.transform.position.y < Camera.main.transform.position.y - Camera.main.orthographicSize)
            {
                LoadNextLevel();
            }
        }

        

        //currentArm.arm.transform.eulerAngles = Vector3.Lerp(currentArm.arm.transform.eulerAngles, new Vector3(0, 0, 360f/sideNumber * currentAngleGoal + .1f * turnDirection), turnRate);
        currentArm.arm.transform.eulerAngles = new Vector3(0, 0, currentPos*360f/sideNumber + ((Mathf.Pow(x, 4)*360f/sideNumber))*turnDirection);
        currentArm.angle = currentArm.arm.transform.eulerAngles.z * Mathf.Deg2Rad;
        currentArm.arm.transform.position = new Vector3(currentArm.x + Mathf.Cos(currentArm.angle)*armLength, currentArm.y + Mathf.Sin(currentArm.angle)*armLength, 1f);

        headObject.transform.position = new Vector3(currentArm.x + Mathf.Cos(currentArm.angle) * armLength * 2f, currentArm.y + Mathf.Sin(currentArm.angle) * armLength * 2f, -1f);
        headObject.transform.eulerAngles = currentArm.arm.transform.eulerAngles;

        for (int i = 0; i < sideNumber; i++)
            Debug.DrawLine(new Vector3(currentArm.x, currentArm.y, 0), new Vector3(currentArm.x + Mathf.Cos((2 * Mathf.PI / sideNumber) * i), currentArm.y + Mathf.Sin((2 * Mathf.PI / sideNumber) * i), 0), Color.red);
        Debug.DrawLine(new Vector3(currentArm.x, currentArm.y, 0), new Vector3(currentArm.x + Mathf.Cos(currentArm.angle)*armLength, currentArm.y + Mathf.Sin(currentArm.angle)*armLength, 0), Color.blue);

        /*if (Input.GetKeyDown(KeyCode.Space))
            tap = true;*/

        if (tap)
        {
            int armPosition1 = 0;
            if (x >= 1 || x < .5f)
                armPosition1 = currentPos;
            else
                armPosition1 = currentAngleGoal;

            RaycastHit rayHit;
            if (!Physics.Raycast(currentArm.joint.transform.position, new Vector3(Mathf.Cos(armPosition1 / (float)sideNumber * 2F * Mathf.PI), Mathf.Sin(armPosition1 / (float)sideNumber * 2F * Mathf.PI), 0), out rayHit, armLength * 2f))
            {
                BuildSnake();
            }
            else if (rayHit.transform.tag == "Goal")
            {
                GameWin();
            }
            else if (rayHit.transform.tag == "PickUp")
            {
                BuildSnake();
                GameObject text = new GameObject();
                text.AddComponent<TextScript>().text = accuracyText[3];
                text.GetComponent<TextScript>().duration = 1f;
                text.GetComponent<TextScript>().position = new Vector3(rayHit.transform.position.x, rayHit.transform.position.y + 2, -2);
                GetComponents<AudioSource>()[1].clip = sfx[5];
                GetComponents<AudioSource>()[1].Play();
                combo += 2;
                score += combo * 30;
                Destroy(rayHit.transform.gameObject);   
            }
            else
            {
                GetComponents<AudioSource>()[1].clip = sfx[0];
                GetComponents<AudioSource>()[1].Play();
                Death();
            }

            if (combo > highestCombo)
                highestCombo = combo;

            tap = false;
        }

        totalScore = ((GetComponent<ControllerScript>().beats > 0) ? (Mathf.RoundToInt(GetComponent<ControllerScript>().score * 100f / GetComponent<ControllerScript>().beats)) : 0);

        int armPosition = 0;
        if (x >= 1 || x < .5f)
            armPosition = currentPos;
        else
            armPosition = currentAngleGoal;
        Debug.DrawLine(new Vector3(currentArm.x, currentArm.y, 0), new Vector3(currentArm.x, currentArm.y, 0) + new Vector3(Mathf.Cos(armPosition / (float)sideNumber * 2F * Mathf.PI), Mathf.Sin(armPosition / (float)sideNumber * 2F * Mathf.PI), 0) * armLength * 2f, Color.green);
    }

    void NewArm()
    {
        Arm newArm = new Arm();
        if (currentArm != null)
        {
            currentArm.accuracy = accuracy;

            //Debug.Log("CP: " + currentPos + " CAG: " + currentAngleGoal);
            int armPosition = 0;
            if (x >= 1 || x < .5f)
                armPosition = currentPos;
            else
                armPosition = currentAngleGoal;
            armPosition = (armPosition+sideNumber) % sideNumber;

            if (currentArm != arms[0])
                currentArm.joint.GetComponent<SpriteRenderer>().color = Color.white;
            currentArm.arm.transform.eulerAngles = new Vector3(0, 0, 360f / sideNumber * armPosition);
            currentArm.angle = currentArm.arm.transform.eulerAngles.z * Mathf.Deg2Rad;
            currentArm.arm.transform.position = new Vector3(currentArm.x + Mathf.Cos(currentArm.angle)*armLength, currentArm.y + Mathf.Sin(currentArm.angle)*armLength, 1f);

            newArm.x = currentArm.x + Mathf.Cos(currentArm.angle) * armLength*2;
            newArm.y = currentArm.y + Mathf.Sin(currentArm.angle) * armLength*2;
            newArm.angle = currentArm.angle + Mathf.PI;


            if (currentArm != arms[0])
            {
                currentArm.arm.GetComponent<SpriteRenderer>().sprite = body[0];
                currentArm.arm.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = body[0];
            }
        }
        else
        {
            newArm.x = startPos.x;
            newArm.y = startPos.y;
        }

        currentArm = newArm;
        arms.Add(currentArm);

        currentArm.joint = new GameObject();
        currentArm.joint.transform.position = new Vector3(currentArm.x, currentArm.y, 0f);
        if (currentArm != arms[0])
            currentArm.joint.AddComponent<SpriteRenderer>().sprite = joint[0];
        currentArm.joint.name = "Joint " + arms.Count;
        currentArm.joint.transform.localScale = Vector3.one * .25f;
        currentArm.joint.AddComponent<SphereCollider>();

        currentArm.arm = new GameObject();
        currentArm.arm.transform.position = new Vector3(currentArm.x + Mathf.Cos(currentArm.angle)*armLength, currentArm.y + Mathf.Sin(currentArm.angle)*armLength, 1f);
        currentArm.arm.AddComponent<SpriteRenderer>().sprite = body[0];
        currentArm.arm.name = "Arm " + arms.Count;
        //currentArm.arm.GetComponent<SpriteRenderer>().color = Color.grey;
        currentArm.arm.transform.localScale = Vector3.one*.5f;
        currentArm.arm.transform.eulerAngles = new Vector3(0, 0, currentArm.angle*Mathf.Rad2Deg);

        arms[0].arm.GetComponent<SpriteRenderer>().sprite = tail;

        GameObject underlay = Instantiate(currentArm.arm);
        underlay.transform.parent = currentArm.arm.transform;
        underlay.transform.localPosition = new Vector3(0,0,-.1f);
        
        if (x > .5f)
            currentPos = (currentPos) % sideNumber;
        else
            currentPos = (currentPos + (sideNumber / 2)) % sideNumber;
        //Debug.Log(currentPos);
        turnDirection *= -1;
    }

    void Death()
    {
        dead = true;
        
        if (headObject)
        {
            GameObject explosion = new GameObject();
            explosion.AddComponent<ExplosionScript>().duration = 60f / bpm;
            for (int i = 0; i < explosions.Length; i++)
                explosion.GetComponent<ExplosionScript>().explosions[i] = explosions[i];
            explosion.GetComponent<ExplosionScript>().position = headObject.transform.position + new Vector3(Random.Range(-.5f, .5f), Random.Range(-.5f, .5f), 0);
            Destroy(headObject);
        }
        else if (arms.Count > 0)
        {
            Arm arm = arms[arms.Count - 1];
            arms.RemoveAt(arms.Count - 1);

            GameObject explosion = new GameObject();
            explosion.AddComponent<ExplosionScript>().duration = 60f / bpm;
            for (int i = 0; i < explosions.Length; i++)
                explosion.GetComponent<ExplosionScript>().explosions[i] = explosions[i];
            explosion.GetComponent<ExplosionScript>().position = arm.arm.transform.position + new Vector3(Random.Range(-.5f, .5f), Random.Range(-.5f, .5f), 0);
            Destroy(arm.arm);

            explosion = new GameObject();
            explosion.AddComponent<ExplosionScript>().duration = 60f / bpm;
            for (int i = 0; i < explosions.Length; i++)
                explosion.GetComponent<ExplosionScript>().explosions[i] = explosions[i];
            explosion.GetComponent<ExplosionScript>().position = arm.joint.transform.position + new Vector3(Random.Range(-.5f, .5f), Random.Range(-.5f, .5f), 0);
            Destroy(arm.joint);
        }
        
        GetComponents<AudioSource>()[1].clip = sfx[0];
        GetComponents<AudioSource>()[1].Play();

        if (arms.Count == 0)
            GameOver();
    }

    void GameOver()
    {
        ResetGame();
    }

    void GameWin()
    {
        gameComplete = true;
        GetComponents<AudioSource>()[0].volume = Mathf.Lerp(GetComponents<AudioSource>()[0].volume, 0.05f, Time.deltaTime);
        if (GetComponents<AudioSource>()[0].volume < .1f)
        {
            if (winStage < 2)
            {
                filter.transform.position = new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, -9f);
                filter.GetComponent<SpriteRenderer>().color = Color.Lerp(filter.GetComponent<SpriteRenderer>().color, Color.black, 2f*Time.deltaTime);

                if (!GetComponents<AudioSource>()[1].isPlaying)
                {
                    GetComponents<AudioSource>()[1].clip = winSounds[winStage];
                    GetComponents<AudioSource>()[1].Play();
                    if (winStage == 1)
                    {
                        reverb = new GameObject();
                        reverb.transform.position = Camera.main.transform.position;
                        reverb.AddComponent<AudioReverbZone>().reverbPreset = AudioReverbPreset.Concerthall;
                    }
                    winStage++;
                }

                Camera.main.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = background;
                Camera.main.transform.GetChild(1).GetComponent<SpriteRenderer>().color = Color.clear;
            }
            else
            {
                Camera.main.transform.GetChild(1).transform.position = new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, -9.1f);
                Camera.main.transform.GetChild(1).GetComponent<SpriteRenderer>().color = Color.Lerp(Camera.main.transform.GetChild(1).GetComponent<SpriteRenderer>().color, Color.white, 4f*Time.deltaTime);
                Camera.main.transform.GetChild(1).transform.localScale = Vector3.one * (Camera.main.orthographicSize / 12) * 1.4f;

                if (reverb)
                    if (!GetComponents<AudioSource>()[1].isPlaying)
                        Destroy(reverb);


                if (!GetComponents<AudioSource>()[1].isPlaying && winStage == 2)
                {
                    if (totalScore < 12500)
                    {
                        GetComponents<AudioSource>()[1].clip = winSounds[2];
                        GetComponents<AudioSource>()[1].Play();
                    }
                    else if (totalScore < 25000)
                    {
                        GetComponents<AudioSource>()[1].clip = winSounds[3];
                        GetComponents<AudioSource>()[1].Play();
                    }
                    else if (totalScore < 50000)
                    {
                        GetComponents<AudioSource>()[1].clip = winSounds[4];
                        GetComponents<AudioSource>()[1].Play();
                    }
                    else
                    {
                        GetComponents<AudioSource>()[1].clip = winSounds[5];
                        GetComponents<AudioSource>()[1].Play();
                    }
                    winStage++;
                }
                if (winStage == 3)
                {
                    
                }
            }
        }
    }

    void ResetGame()
    {
        Application.LoadLevel(Application.loadedLevel);
    }

    void BuildSnake()
    {
        currentArm.joint.GetComponent<SpriteRenderer>().sprite = joint[accuracy];
        NewArm();
        GameObject text = new GameObject();
        text.AddComponent<TextScript>().text = accuracyText[accuracy - 1];
        text.GetComponent<TextScript>().duration = 1f;
        text.GetComponent<TextScript>().position = new Vector3(currentArm.x, currentArm.y + 1, -2);
        GetComponents<AudioSource>()[1].clip = sfx[accuracy + 1];
        GetComponents<AudioSource>()[1].Play();
        combo++;
        score += combo * 5 * accuracy;
        rotations = 0;

        beatAccuracy[accuracy-1]++;
    }

    void LoadNextLevel()
    {
        if (currentLevel > 0)
            countDown = 4;

        /*if (levelObject)
            Destroy(levelObject);*/

        currentLevel++;

        if (currentArm.joint.transform.position.x > Camera.main.transform.position.x + Camera.main.orthographicSize * 16f / 9f)
            Camera.main.GetComponent<CameraScript>().target = new Vector2(Camera.main.GetComponent<CameraScript>().transform.position.x + 2.09f*Camera.main.orthographicSize * 16f / 9f, Camera.main.GetComponent<CameraScript>().transform.position.y);
        if (currentArm.joint.transform.position.x < Camera.main.transform.position.x - Camera.main.orthographicSize * 16f / 9f)
            Camera.main.GetComponent<CameraScript>().target = new Vector2(Camera.main.GetComponent<CameraScript>().transform.position.x - 2.09f*Camera.main.orthographicSize * 16f / 9f, Camera.main.GetComponent<CameraScript>().transform.position.y);
        if (currentArm.joint.transform.position.y > Camera.main.transform.position.y + Camera.main.orthographicSize)
            Camera.main.GetComponent<CameraScript>().target = new Vector2(Camera.main.GetComponent<CameraScript>().transform.position.x, Camera.main.GetComponent<CameraScript>().transform.position.y + 2.07f*Camera.main.orthographicSize);
        if (currentArm.joint.transform.position.y < Camera.main.transform.position.y - Camera.main.orthographicSize)
        Camera.main.GetComponent<CameraScript>().target = new Vector2(Camera.main.GetComponent<CameraScript>().transform.position.x, Camera.main.GetComponent<CameraScript>().transform.position.y - 2.07f*Camera.main.orthographicSize);

        levelObject = Instantiate(levelPrefabs[currentLevel-1]);
        levelObject.transform.position = new Vector3(Camera.main.GetComponent<CameraScript>().target.x, Camera.main.GetComponent<CameraScript>().target.y, 0);
        GameObject border = Instantiate(borders);
        border.transform.position = levelObject.transform.position;
        border.transform.parent = levelObject.transform;
    }
}
