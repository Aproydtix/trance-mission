# Trance Mission
Made in Unity using C#.

Global Game Jam 2018 "Transmission", rhythm game.
The game is about unfolding a snake as if it was a folding ruler by pressing Space to the beat.
You need to time your presses, plan your route, and pick up items to achieve a higher score.

I programmed everything (apart from the fractal effect) in the main game. The snake, level systems, pickups, score, effects, and tools for building the levels.

## Credits
### Art
- Sofie Gjengstø
- Simen Leander Ingebrigtsen
- Hauk Seim Flåøien
- Haakon Aarstein
### Programming
- Eirik Hiis-Hauge
- Dexter Andre Osiander
- Åsmund Kløvstad
- Haakon Aarstein
- Einar Julius Olafarson
### Level Design
- Åsmund Kløvstad
- Einar Julius Olafarson
### Music
- Hauk Seim Flåøien

## Build
A build can be found as a .zip file in the Downloads section.

## Video Demo
https://youtu.be/_x7OPBrf3LI

## Screenshots
![Screenshot1](Screenshot1.png)
![Screenshot2](Screenshot2.png)
![Screenshot3](Screenshot3.png)